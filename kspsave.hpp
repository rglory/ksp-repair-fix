#ifndef KSPSAVE_HPP
#define KSPSAVE_HPP

#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <unordered_map>

using pairs = std::unordered_multimap<std::string,std::string>;

struct sitem {
    std::string m_key;

    virtual ~sitem() = default;

    virtual void save( std::ostream &out, int offset ) const = 0;
    virtual void print( std::ostream &out, int offset, int maxLevel ) const = 0;
    virtual std::string print_if( int offset, const pairs &p ) const = 0;

    static void save_offset( std::ostream &out, int offset );
};

struct svalue : sitem {

    std::string m_value;

    virtual void save( std::ostream &out, int offset ) const;
    virtual void print( std::ostream &out, int offset, int maxLevel ) const;
    virtual std::string print_if( int offset, const pairs &p ) const;
};

struct sgroup : sitem {

    std::vector<std::unique_ptr<sitem>> m_items;

    virtual void save( std::ostream &out, int offset ) const;
    virtual void print( std::ostream &out, int offset, int maxLevel = -1 ) const;
    virtual std::string print_if( int offset, const pairs &p ) const;

    sitem *find( const std::string &name );

    std::string &findValue( const std::string &key );
    sgroup &findGroup( const std::string &key );
};

sgroup read( std::istream &in );



#endif // KSPSAVE_HPP
