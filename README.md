# KSP Repair Fix

Small APP to fix ksp save file for vessel ID mismatch.

# Build

Clone repository or unpack downloaded zip file. Create build directory. Inside run commans:

    cmake ..
    make
Executable will be in the build dir. To build Windows executable on Linux run cmake with arguments:

    cmake .. -DCMAKE_TOOLCHAIN_FILE=../Toolchain-Ubuntu-mingw64.cmake

# How to fix save file

Accept Satellite (Probe) repair mission. Save your game. Place ksp_save_fix (ksp_save_fix.exe for Windows) somewhere. Easiest would be to put into KSP save directory for this game. Run it with save file .sfs as argument (do not forget to make a copy just in case). If you put it somewhere else provide save file name with path.

If you have a broken contract you would see output something like:

./ksp_save_fix test1.sfs

```
Processing vessels:
        probe "Aging Minmus MLA Satellite F-8NQ" - 2593076007
        probe "Aging Minmus MLA Satellite 6-L46" - 604665979
        probe "Damaged Minmus MLA Satellite 9N3F" - 4255187660
        probe "Aging Kerbin Rec-N Satellite 1-429G" - 3789998851
                loaded 4 probes
Processing contracts:
        active contract: 96698f1d-0f81-4b99-8ec8-2e9ac7c2506e
                constructionVslId matches
                vesselPersistentId mismatch, fixed
Saving changes:
        ok
```
You can download zipped compiled executables for windows and ubuntu here https://drive.google.com/drive/folders/1GdLa6IjfOJXVlc4KjGb3zpfxUa8ULKUm?usp=sharing

Now test1.sfs is fixed.
Enjoy
