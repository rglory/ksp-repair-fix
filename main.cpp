#include <iostream>
#include <fstream>
#include <algorithm>

#include "kspsave.hpp"

int main(int argc, char **argv )
{
    if( argc < 2 ) {
        std::cout << "usage: " << argv[0] << " <savefile.sfs>" << std::endl;
        return 100;
    }
    
    std::ifstream in( argv[1] );
    if( !in ) {
        std::cout << "Error: cannot open file: " << argv[1] << std::endl;
        return 102;
    }
    auto game = read( in );
    in.close();

    std::cout << "Processing vessels:\n";
    std::unordered_multimap<std::string,std::string> vessels;
    try {
        auto &fstate = game.findGroup( "FLIGHTSTATE" );
        for( const auto &it : fstate.m_items ) {
            if( it->m_key != "VESSEL" ) continue;
            sgroup *vessel = static_cast<sgroup *>( it.get() );
            if( vessel->findValue( "type" ) != "Probe" ) continue;
            auto name  = vessel->findValue( "name" );
            auto id    = vessel->findValue( "persistentId" );
            std::cout << "\tprobe \"" << name << "\" - " << id << std::endl;
            vessels.emplace( name, id );
        }
        std::cout << "\t\tloaded " << vessels.size() << " probes\n";
    }
    catch( const std::exception &err ) {
        std::cout << "\tError: cannot process save file:" << err.what() << std::endl;
        return 101;
    }

    bool changed = false;
    std::cout << "Processing contracts:\n";
    auto it = std::find_if( game.m_items.begin(), game.m_items.end(), []( const auto &p ) {
            if( p->m_key != "SCENARIO" ) return false;
            if( auto *g = dynamic_cast<sgroup *>( p.get() ) )
                return g->findValue( "name" ) == "ContractSystem";
            return false;
        } );
    if( it == game.m_items.end() ) {
        std::cout << "\tError: cannot find ContractSystem SCENARIO" << std::endl;
        return 102;
    }
    auto &contracts = static_cast<sgroup*>(it->get() )->findGroup( "CONTRACTS" );
    for( const auto &item : contracts.m_items ) {
        if( item->m_key != "CONTRACT" ) continue;
        auto contract = static_cast<sgroup *>( item.get() );
        if( contract->findValue( "type" ) != "OrbitalConstructionContract" ) continue;
        if( contract->findValue( "state" ) != "Active" ) continue;
        std::cout << "\tactive contract: " <<  contract->findValue( "guid" ) << std::endl;
        auto vname = contract->findValue( "vesselName" );
        auto count = vessels.count( vname );
        if( count == 0 ) {
            std::cout << "\t\tcannot validate - vessel \"" << vname << "\" not found" << std::endl;
            continue;
        }
        if( count > 1 ) {
            std::cout << "\t\tcannot validate - more than one vessel \"" << vname << "\" found" << std::endl;
            continue;
        }
        auto realId = vessels.find( vname )->second;
        auto &vid = contract->findValue( "constructionVslId" );
        if( vid != realId ) {
            std::cout << "\t\tconstructionVslId mismatch, fixed\n";
            vid = realId;
            changed = true;
        } else
            std::cout << "\t\tconstructionVslId matches\n";

        auto it = std::find_if( contract->m_items.begin(), contract->m_items.end(), []( const auto &p ) {
            if( p->m_key != "PARAM" ) return false;
            if( auto *g = dynamic_cast<sgroup *>( p.get() ) )
                return g->findValue( "name" ) == "ConstructionParameter";
            return false;
        } );
        if( it == contract->m_items.end() ) {
            std::cout << "\t\twarning - cannot find PARAM ConstructionParameter\t";
            continue;
        }
        auto param = static_cast<sgroup *>( it->get() );
        auto &vvid = param->findValue( "vesselPersistentId" );
        if( vvid != realId ) {
            std::cout << "\t\tvesselPersistentId mismatch, fixed\n";
            vvid = realId;
            changed = true;
        } else
            std::cout << "\t\tvesselPersistentId matches\n";
    }
    if( changed ) {
        std::cout << "Saving changes:\n";
        std::ofstream save( argv[1] );
        game.save( save, 0 );
        std::cout << "\tok" << std::endl;

    }
}

