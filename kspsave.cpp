#include "kspsave.hpp"

#include <sstream>
#include <algorithm>

void sitem::save_offset( std::ostream &out, int offset )
{
    while( offset-- ) out << '\t';
}

void svalue::save( std::ostream &out, int offset ) const
{
    save_offset( out, offset );
    out << m_key << " = " << m_value << std::endl;
}

void svalue::print( std::ostream &out, int offset, int maxLevel ) const
{
    if( maxLevel != -1 ) return;
    for( int i = 0; i < offset; ++i ) out << "   ";
    out << m_key << " = \"" << m_value << "\"\n";
}
    
std::string svalue::print_if( int offset, const pairs &p ) const
{
    bool matched = false;
    for( auto s : { std::string(), m_key } ) {
        auto r = p.equal_range( s );
        for( auto it = r.first; not matched and it != r.second; ++it ) {
            if( it->second.empty() ) matched = true;
            if( m_value.find( it->second ) != std::string::npos ) matched = true;
        }
        if( matched ) break;
    }
    if( not matched ) return std::string();
    std::ostringstream r;
    print( r, offset, -1 );
    return r.str();
}

void sgroup::save( std::ostream &out, int offset ) const
{
    save_offset( out, offset );
    out << m_key << std::endl;
    save_offset( out, offset );
    out << '{' << std::endl;

    for( const auto &it : m_items )
        it->save( out, offset + 1 );

    save_offset( out, offset );
    out << '}' << std::endl;
}

void sgroup::print( std::ostream &out, int offset, int maxLevel ) const
{
    if( offset == maxLevel ) return;
    for( int i = 0; i < offset; ++i ) out << "   ";
    out << m_key << std::endl;
    for( const auto &it : m_items )
        it->print( out, offset + 1, maxLevel );
}

std::string sgroup::print_if( int offset, const pairs &p ) const
{
    std::string r( offset * 3, ' ' );
    r += m_key + "\n";
    auto el = r.length();
    for( const auto &it : m_items ) 
        r += it->print_if( offset + 1, p );
    if( r.length() != el ) return r;
    if( p.count( m_key ) ) return r;
    return std::string();
}

sitem *sgroup::find( const std::string &key ) 
{
    auto it = std::find_if( m_items.begin(), m_items.end(), [key]( const auto &p ) {
                return p->m_key == key;
              } );
    return it == m_items.end() ? nullptr : it->get();
}


std::string &sgroup::findValue( const std::string &key )
{
    auto p = find( key );
    if( not p ) throw std::runtime_error( key + " value not found in " + m_key );
    if( auto v = dynamic_cast<svalue *>( p ) ) return v->m_value;
    throw std::runtime_error( key + " is not a value in " + m_key );
}

sgroup &sgroup::findGroup( const std::string &key )
{
    auto p = find( key );
    if( not p ) throw std::runtime_error( key + " group not found in " + m_key );
    if( auto v = dynamic_cast<sgroup *>( p ) ) return *v;
    throw std::runtime_error( key + " is not a group in " + m_key );
}

sgroup read( std::istream &in )
{
    sgroup game;
    std::vector<sgroup *> stack;

    std::string s;
    while( std::getline( in, s ) ) {
        auto p = s.find_first_not_of( '\t' );
        s.erase( 0, p );
        p = s.find( '=' );
        if( p == std::string::npos ) {
            if( s == "{" ) continue;
            if( s == "}" ) {
                stack.pop_back();
                continue;
            }
            if( stack.empty() ) stack.push_back( &game );
            else {
                auto g = std::make_unique<sgroup>();
                auto ptr = g.get();
                stack.back()->m_items.push_back( std::move( g ) );
                stack.push_back( ptr );
            }
            stack.back()->m_key = s;
        } else {
            auto v = std::make_unique<svalue>();
            v->m_key = s.substr( 0, p - 1 );
            v->m_value = s.substr( p + 2 );
            stack.back()->m_items.push_back( std::move( v ) );
        }
    }
    return game;
}

